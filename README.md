# bethel-angular-exercise

# How to run the project
`npm i`

`npm start`

# Changing languages
You can set the language with a query parameter:
```
http://localhost:8080/#/user-list?lang={en,es,pt}
```
If any given key is missing in a translation, it'll fall back to the English string
If any given translation doesn't exist, it'll fall back to English translations

# Notes
This project used the most excellent https://github.com/vsternbach/angularjs-typescript-webpack as a starter project
