import 'angular-translate';
import 'angular-translate-loader-static-files';
import { NgModule } from 'angular-ts-decorators';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddressDetailComponent } from './shared/address-detail/address-detail.component';
import { UserDataFieldComponent } from './shared/user-data-field/user-data-field.component';
import './styles.css';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserRowDirective } from './user-row.directive';
import { UserService } from './user.service';

@NgModule({
  id: 'AppModule',
  imports: [AppRoutingModule, 'pascalprecht.translate'],
  declarations: [
    AddressDetailComponent,
    AppComponent,
    UserDetailComponent,
    UserListComponent,
    UserRowDirective,
    UserDataFieldComponent,
  ],
  providers: [UserService],
  bootstrap: [AppComponent],
})
export class AppModule {
  static config($translateProvider) {
    'ngInject';
    $translateProvider.useStaticFilesLoader({
      prefix: 'i18n/',
      suffix: '.json',
    });
    $translateProvider.registerAvailableLanguageKeys(['en', 'es', 'pt']);
    $translateProvider.preferredLanguage('en');
    $translateProvider.fallbackLanguage('en');
  }
}
