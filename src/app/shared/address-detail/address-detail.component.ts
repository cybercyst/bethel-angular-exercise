import { Component, Input } from 'angular-ts-decorators';
import { Address } from '../types/address.interface';

@Component({
  selector: 'address-detail',
  template: require('./address-detail.component.html'),
  styles: [require('./address-detail.component.scss')],
})
export class AddressDetailComponent {
  @Input() address: Address;

  /*@ngInject*/
  constructor() {}
}
