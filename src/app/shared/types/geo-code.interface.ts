export interface GeoCode {
  lat: string;
  lng: string;
}
