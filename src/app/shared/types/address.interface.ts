import { GeoCode } from './geo-code.interface';

export interface Address {
  city: string;
  geo: GeoCode;
  street: string;
  suite: string;
  zipcode: string;
}
