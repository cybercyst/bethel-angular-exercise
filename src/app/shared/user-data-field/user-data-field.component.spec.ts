import { ComponentFixture, DebugElement, TestBed } from 'angularjs-testbed';
import { UserDataFieldComponent } from './user-data-field.component';

describe('UserDataFieldComponent', () => {
  let component: UserDataFieldComponent;
  let fixture: ComponentFixture<UserDataFieldComponent>;
  let debugElement: DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [UserDataFieldComponent],
    });

    fixture = TestBed.createComponent(UserDataFieldComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement;
    fixture.detectChanges();
  });

  afterEach(() => TestBed.resetTestingModule());

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
