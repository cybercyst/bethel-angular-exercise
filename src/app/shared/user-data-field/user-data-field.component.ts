import { Component, Input } from 'angular-ts-decorators';

@Component({
  selector: 'user-data-field',
  template: require('./user-data-field.component.html'),
  styles: [require('./user-data-field.component.scss')],
})
export class UserDataFieldComponent {
  @Input() label: string;
  @Input() name: string;
  @Input() value: any;

  /*@ngInject*/
  constructor() {}
}
