import { StateService } from '@uirouter/angularjs';
import { Component, OnInit } from 'angular-ts-decorators';
import { User } from '../shared/types/user.interface';
import { UserService } from '../user.service';

@Component({
  selector: 'user-detail',
  template: require('./user-detail.component.html'),
  styles: [require('./user-detail.component.scss')],
})
export class UserDetailComponent implements OnInit {
  user: User;

  /*@ngInject*/
  constructor(private $state: StateService, private userService: UserService) {}

  ngOnInit() {
    const userId = +this.$state.params.userId;
    this.getUserById(userId);
  }

  getUserById(userId: number) {
    this.userService.getUserById(userId).then(user => {
      this.user = user;
    });
  }
}
