import { Component, OnInit } from 'angular-ts-decorators';
import { User } from '../shared/types/user.interface';
import { UserService } from '../user.service';

@Component({
  selector: 'user-list',
  template: require('./user-list.component.html'),
  styles: [require('./user-list.component.scss')],
})
export class UserListComponent implements OnInit {
  users: User[];

  /*@ngInject*/
  constructor(private userService: UserService) {}

  ngOnInit() {
    this.getAllUsers();
  }

  getAllUsers() {
    this.userService.getAllUsers().then(users => {
      this.users = users.sort(this.sortUsersByNameDescending);
    });
  }

  private sortUsersByNameDescending(a: User, b: User) {
    return a.name > b.name ? -1 : 1;
  }
}
