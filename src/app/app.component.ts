import { LocationServices } from '@uirouter/angularjs';
import { Component, Inject, OnInit } from 'angular-ts-decorators';

@Component({
  selector: 'app-root',
  template: require('./app.component.html'),
  styles: [require('./app.component.scss')],
})
export class AppComponent implements OnInit {
  /*@ngInject*/
  constructor(
    @Inject('$location') private $location,
    @Inject('$translate') private $translate,
  ) {}

  ngOnInit() {
    const lang = this.$location.search().lang;
    if (lang) {
      this.$translate.use(lang);
    }
  }
}
