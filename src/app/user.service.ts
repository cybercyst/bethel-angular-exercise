import { IHttpService, IPromise } from 'angular';
import { Injectable } from 'angular-ts-decorators';
import { environment } from '../environments/environment';
import { User } from './shared/types/user.interface';

@Injectable('userService')
export class UserService {
  private apiUrl = `${environment.apiUrl}/users`;

  /*@ngInject*/
  constructor(private $http: IHttpService) {}

  getAllUsers(): IPromise<User[]> {
    return this.$http.get<User[]>(this.apiUrl).then(response => {
      return response.data;
    });
  }

  getUserById(userId: number): IPromise<User> {
    return this.$http
      .get<User>(`${this.apiUrl}/${userId}`)
      .then(response => response.data);
  }
}
