import { Directive } from 'angular-ts-decorators';

@Directive({
  selector: '[user-row]',
  template: `
      <td><a ui-sref="user({ userId: user.id })">{{ user.name }}</a></td>
      <td>{{ user.username }}</td>
      <td><a href="mailto:{{ user.email }}">{{user.email}}</a></td>
      <td><address-detail address="user.address"></address-detail></td>`,
})
export class UserRowDirective {
  /*@ngInject*/
  constructor() {}
}
